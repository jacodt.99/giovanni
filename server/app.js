const express = require('express');
const session = require('express-session');
const app = express();


const WebSocket = require('ws');

const wss = new WebSocket.Server({
    port: 8080
});

var id = 0;
var lookup = {};
let data = new Object();
wss.on('connection', (ws) => {
    wss.id = id++;
    console.log("wss connection established: ip=" + ws._socket.remoteAddress + " port=" + ws._socket.remotePort);
    ws.on('message', (message) => {
        //console.clear();
        //console.log(message);
        message = JSON.parse(message);
        data[wss.id] = message;
        console.clear();
        console.log(data);
    });
});

function broadcastData() {
    wss.clients.forEach(function each(client) {
          client.send(JSON.stringify(data));
    });
}
setInterval(broadcastData, 50);





