import {game} from "../index.js";

export default class InputHandler {
    constructor(mySpaceship) {
        let aDown = false;
        let dDown = false;
      document.addEventListener("keydown", event => {


        switch (event.keyCode) {
            case 87: // w
                mySpaceship.accelerating = true;
                break;
            case 65: // a
                aDown = true;
                mySpaceship.rotatingRight = false;
                mySpaceship.rotatingLeft = true;
                break;
            case 68: // d
                dDown = true;
                mySpaceship.rotatingLeft = false;
                mySpaceship.rotatingRight = true;
                break;
            case 32: // space
                mySpaceship.fireing = true;
                break;
        }
      });
  
      document.addEventListener("keyup", event => {
        switch (event.keyCode) {
            case 65: // a
                if(dDown) game.mySpaceship.rotatingRight = true; //1. bug patch
                mySpaceship.rotatingLeft = false;
                aDown = false;
                break;
            case 68: // d
                if(aDown) game.mySpaceship.rotatingLeft = true; //1. bug patch
                mySpaceship.rotatingRight= false;
                dDown = false;
                break;
            case 87: // w
                mySpaceship.accelerating = false;
                break;
            case 32: // space
                mySpaceship.fireing = false;
                break;
        }
      });
    }
  }

