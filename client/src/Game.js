import MySpaceship from "./MySpaceship.js";
import OnlineSpaceship from "./OnlineSpaceship.js";
import InputHandler from "./InputHandler.js";

let canvas = document.getElementById("gameScreen");

const GAMESTATE = {
    PAUSED: 0,
    RUNNING: 1,
    MENU: 2,
    GAMEOVER: 3
  };

export default class Game {
    constructor(width, height){
        this.width = width;
        this.height = height;
        this.gamestate = GAMESTATE.MENU;

        this.mySpaceship = new MySpaceship(this);
        new InputHandler(this.mySpaceship);
        this.onlineSpaceships = [];
    }

    start(){
    }
    
    wsUpdate(data){
        for (var key in data){
            if (!this.onlineSpaceships[key])
                this.onlineSpaceships[key] = new OnlineSpaceship(this, "gay", key, data[key].spaceship.position.x, data[key].spaceship.position.y);
          } 
    }

    update(deltaTime){
        //console.log(this.onlineSpaceships);
        this.mySpaceship.update(deltaTime);
        this.mySpaceship.projectiles.forEach(element => element.update(deltaTime));
        for (var key in this.onlineSpaceships) this.onlineSpaceships[key].update(deltaTime);
    }
    draw(ctx){
        this.mySpaceship.draw(ctx);
        this.mySpaceship.projectiles.forEach(element => element.draw(ctx));
        for (var key in this.onlineSpaceships) this.onlineSpaceships[key].draw(ctx);
        canvas.style.backgroundPosition = this.mySpaceship.position.x + "px " + this.mySpaceship.position.y + "px";
    }
}