import {game} from "../index.js";

const tickrate = 50;
const wss = new WebSocket("ws://localhost:8080");

window.setInterval(sendData, tickrate);
name = "life";
function sendData() {
  wss.send(JSON.stringify({
    player: name,
    spaceship:{ 
      position:{
        x: Math.round(game.mySpaceship.position.x),
        y: Math.round(game.mySpaceship.position.y)
      },
      projectiles: parseProjectiles()
    }
  }));
}

function parseProjectiles() {
    var projectilePosition = [];
    game.mySpaceship.projectiles.forEach(element => {
      projectilePosition.push({x: Math.round(element.position.x), y: Math.round(element.position.y)})
    });
    return projectilePosition;
}

export let wssData = [];
wss.onmessage = function(e){
    var data = JSON.parse(e.data);
    //console.log(data);

    for (var key in data){
      if (key !== name)
          wssData[key] = data[key];
    }
    game.wsUpdate(wssData);
 }
