import Projectile from "./Projectile.js"
import { wssData } from "./Websocket.js";

export default class OnlineSpaceship{

    constructor(game, type, player, x, y) {
        this.sprites = {
            normal: document.getElementById("spaceship"),
            accelerating: [
                document.getElementById("acceleratingSpaceship1"),
                document.getElementById("acceleratingSpaceship2"),
                document.getElementById("acceleratingSpaceship3"),
                document.getElementById("acceleratingSpaceship4"),
            ]
        }
        this.position = {
          x: x,
          y: y
        };
        this.player = player;
        this.type = type;
        this.projectiles = [];
        this.game = game;
    }

    update(deltaTime){
        console.log(this.player)
        //console.log(wssData)
        //console.log(wssData)
        this.position.x = wssData[this.player].spaceship.position.x;
        this.position.y = wssData[this.player].spaceship.position.y;
    }

    draw(ctx){
       // console.log(this.position.x)
        ctx.drawImage(this.sprites.normal, this.position.x, this.position.y);
    }
}