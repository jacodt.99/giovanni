import Projectile from "./Projectile.js"

export default class MySpaceship{

    constructor(game, type, playership) {
        this.sprites = {
            normal: document.getElementById("spaceship"),
            accelerating: [
                document.getElementById("acceleratingSpaceship1"),
                document.getElementById("acceleratingSpaceship2"),
                document.getElementById("acceleratingSpaceship3"),
                document.getElementById("acceleratingSpaceship4"),
            ]
        }
        this.position = {
          x: 0,
          y: 0
        };
        this.type = type;
        this.maxSpeed = 1;
        this.speed = 0;
        this.angleDeg = 0;
        this.projectiles = [];
        this.game = game;
        this.playership = playership;
        //state
        this.rotatingRight = false;
        this.rotatingLeft = false;
        this.accelerating = false;
        this.fireing = false;
        this.frame = 0;
    }

    accelerate(){
        this.speed += 0.05;
        if(this.speed > this.maxSpeed) this.speed = this.maxSpeed;
    }
    decelerate(){
        this.speed -= 0.05;
        if(this.speed < 0) this.speed = 0;
    }
    rotateLeft(){
        this.angleDeg += 3;
        if (this.angleDeg > 360 || this.angleDeg < -360) this.angleDeg = 0;
    }
    rotateRight(){
        this.angleDeg -= 3;
        if (this.angleDeg > 360 || this.angleDeg < -360) this.angleDeg = 0;
    }
    fire(){
        this.projectiles.push(new Projectile(this.game, 
            this.position.x + Math.sin(this.angleDeg / 180 * Math.PI) * this.sprites.normal.height / 2 + 2 , 
            this.position.y + Math.cos(this.angleDeg / 180 * Math.PI) * this.sprites.normal.height / 2 + 2 ,
            this.angleDeg));
    }

    update(deltaTime){
        this.position.x += (this.speed * Math.sin(this.angleDeg / 180 * Math.PI)) * 10
        this.position.y += (this.speed * Math.cos(this.angleDeg / 180 * Math.PI)) * 10
        if (this.rotatingRight) this.rotateRight();
        if (this.rotatingLeft) this.rotateLeft();
        if (this.accelerating) this.accelerate();
        else this.decelerate();
        if (this.fireing) this.fire();
    }

    draw(ctx){

        this.frame++;
        if (this.frame >= 4) this.frame = 0;
        ctx.save(); // save current state
        ctx.translate(ctx.canvas.clientWidth / 2, ctx.canvas.clientHeight / 2);
        ctx.rotate(-this.angleDeg * Math.PI / 180); // rotate  
        if (this.accelerating)
            ctx.drawImage(this.sprites.accelerating[this.frame], -this.sprites.normal.width / 2, -this.sprites.normal.height / 2);
        else 
            ctx.drawImage(this.sprites.normal, -this.sprites.normal.width / 2, -this.sprites.normal.height / 2);
        ctx.restore();

    }
}