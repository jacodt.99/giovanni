export default class Projectile {
    constructor(game,x, y, angleDeg, type) {
        this.firedPosition = {
          x: x,
          y: y
        };
        this.position = {
          x: x,
          y: y
        };
        this.angleDeg = angleDeg;
        this.speed = 1.5;
        this.maxDistance = 800;
        this.type = type;
        this.game = game;
        this.image = document.getElementById("fire");  
      }

      update(deltaTime){
        this.position.x += (this.speed * Math.sin(this.angleDeg / 180 * Math.PI)) * 10;
        this.position.y += (this.speed * Math.cos(this.angleDeg / 180 * Math.PI)) * 10;

        if (this.position.x > this.firedPosition.x + this.maxDistance ||
            this.position.y > this.firedPosition.y + this.maxDistance ||
            this.position.x < this.firedPosition.x - this.maxDistance ||
            this.position.y < this.firedPosition.y - this.maxDistance)
            this.game.mySpaceship.projectiles.splice(this.game.mySpaceship.projectiles.indexOf(this), 1);
            
      }

      draw(ctx){
        ctx.drawImage(this.image,  
            ctx.canvas.clientWidth / 2 - this.position.x + this.game.mySpaceship.position.x,
            ctx.canvas.clientHeight / 2 - this.position.y + this.game.mySpaceship.position.y);
      }
}