import Game from "./src/Game.js";
//import wss from "./src/Websocket.js";

document.addEventListener('contextmenu', event => event.preventDefault());

let canvas = document.getElementById("gameScreen");
let ctx = canvas.getContext("2d");

window.addEventListener('resize', resizeCanvas);
window.addEventListener('load', resizeCanvas);

// per fare si che il canvas ri resize correttamente
function resizeCanvas() {
  ctx.canvas.width = window.innerWidth;
  ctx.canvas.height = window.innerHeight;
  //console.log("canvas resized!");
}

const GAME_WIDTH = 10000;
const GAME_HEIGHT = 10000;

export let game = new Game(GAME_WIDTH, GAME_HEIGHT);

let lastTime = 0;

function gameTick(timestamp) {
  let deltaTime = timestamp - lastTime;
  lastTime = timestamp;

  ctx.clearRect(0, 0, canvas.width, canvas.height);
  game.update(deltaTime);
  game.draw(ctx);
  requestAnimationFrame(gameTick);
  
  debug();

}

requestAnimationFrame(gameTick);

function debug() {
  document.getElementById("debug").innerHTML = "spX:" + game.mySpaceship.position.x
  + "<br>" + "spY:" + game.mySpaceship.position.y
  + "<br>" + "spA" + game.mySpaceship.angleDeg;
  //console.log(game.mySpaceship.projectiles.length);
}
